$(document).ready(function(){
// Mobile Menu
var bar = function(){
  $('header nav').slideToggle();
  $(this).toggleClass("close-bt-hamburguer");
};

$('.bar-mobile').bind("click", bar);

$('.menu-mobile a').click(function(){
  $(this).siblings('ul').slideToggle();
});

$('header.inner .bar-mobile').click(function(){
  $('.bar-mobile span').toggleClass('toggle-white')
})

$( '.sldr' ).each( function() {
  var th = $( this );
  th.sldr({
    focalClass    : 'focalPoint',
    offset        : th.width() / 2,
    sldrWidth     : 'responsive',
    nextSlide     : th.nextAll( '.sldr-nav.next:first' ),
    previousSlide : th.nextAll( '.sldr-nav.prev:first' ),
    selectors     : th.nextAll( '.selectors:first' ).find( 'li' ),
    toggle        : th.nextAll( '.captions:first' ).find( 'div' ),
    sldrInit      : sliderInit,
    sldrStart     : slideStart,
    sldrComplete  : slideComplete,
    sldrLoaded    : sliderLoaded,
    sldrAuto      : false,
    sldrTime      : 5000,
    hasChange     : true
  });
});
  /**
   * Sldr Callbacks
   */

  /**
   * When the sldr is initiated, before the DOM is manipulated
   * @param {object} args the slides, callback, and config of the slider
   * @return null
   */
   function sliderInit( args ) {

   }

  /**
   * When individual slides are loaded
   * @param {object} args the slides, callback, and config of the slider
   * @return null
   */
   function slideLoaded( args ) {

   }

  /**
   * When the full slider is loaded, after the DOM is manipulated
   * @param {object} args the slides, callback, and config of the slider
   * @return null
   */
   function sliderLoaded( args ) {

   }

  /**
   * Before the slides change focal points
   * @param {object} args the slides, callback, and config of the slider
   * @return null
   */
   function slideStart( args ) {

   }

  /**
   * After the slides are done changing focal points
   * @param {object} args the slides, callback, and config of the slider
   * @return null
   */
   function slideComplete( args ) {

   }





  // Каруселька
  // Documentation: http://www.owlcarousel.owlgraphic.com/docs/api-options.html
  $("#owl-carousel").owlCarousel({
    items: 1, 
    loop: true,
    autoplay:false,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
    navText: [""],
    dots: true
  });
  $(".owl-carousel").owlCarousel({
    items: 1, 
    loop: true,
    autoplay:false,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
    navText: [""],
    dots: true,
    video: true
  });

  $(".owl-carousel-videos").owlCarousel({
    items: 1,
    stagePadding: 50,
    margin: 20, 
    loop: false,
    video:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
videoWidth: false, // Default false; Type: Boolean/Number
videoHeight: false, // Default false; Type: Boolean/Number
nav:true,
navText: [""],
dots: false,
video:true
});



  // var resizeId;
  // $(window).resize(function() {
  //     clearTimeout(resizeId);
  //     resizeId = setTimeout(mobileSearch, 500);
  // });

var mobileSearch = function(){
   if($(window).width() > 1100){

    $( "#search_submit" ).on('click',function(){
      if($(this).attr('data-toggle') == 'off'){
        $(this).attr('data-toggle', 'on')
        $('.menu-link').toggleClass('visibility-no')
        setTimeout(function(){
          $('input#search').toggle( "slide", { direction: "right" }, 300 )
        },500)
      }else if($(this).attr('data-toggle') == 'on'){
        $(this).attr('data-toggle', 'off')
        setTimeout(function(){
          $('.menu-link').toggleClass('visibility-no')
        },500)
        $('input#search').toggle( "slide", { direction: "right" }, 300 )
      }
    })
  }else{
    $( "#search_submit" ).on('click',function(){
      $('.menu-link').toggleClass('vis-high')
      if($(this).attr('data-toggle') == 'off'){
        $(this).attr('data-toggle', 'on')
        setTimeout(function(){
          $('input#search').toggle( "slide", { direction: "right" }, 300 )
        },500)
      }else if($(this).attr('data-toggle') == 'on'){
        $(this).attr('data-toggle', 'off')
        $('input#search').toggle( "slide", { direction: "right" }, 300 )
      }
    })
  }
}
mobileSearch();


// Аякс отправка формы 
 // Документация https://api.jquery.com/category/ajax/
//  $("#callback").submit(function(){
//   $.ajax({
// type: "POST",
// url: "mail.php",
// data: $("#callback").serialize()
//   }).done(function() {
//     alert("Спасибо за заявку");
//   });
//   return false;
//  });



// Loader
// $(window).on('load', function(){

//   $('#loader').delay(2000).fadeOut('slow');

// });


// Кроссбраузерность
var isMobile = {
  Android: function() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function() {
    return navigator.userAgent.match(/IEMobile/i);
  },
  ie: function(){
    return navigator.userAgent.match(/Trident/i);
  },
  any: function() {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

// Opera-mini
if(isMobile.Opera()){
      // $('.cart, .search').css('display', 'none');
      // $('#menu').css({'display': 'table', 'padding-top': '16px'});
      // $('#menu ul').css({'display': 'table-cell', 'vertical-align': 'middle', 'padding-top': '15px'});
    };
  });
