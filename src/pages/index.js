import React from 'react'
import Link from 'gatsby-link'
import Home from './home'

class IndexPage extends React.Component {

  render() {
    return (
      <Home/>
    )
  }
}

export default IndexPage
