import React from 'react'
import Helmet from 'react-helmet'

import { HomeApi } from '../api/HomeApi'
import Footer from '../components/footer'
import Slide from '../components/Slide'
import Popular from '../components/Popular'
import { MainApi } from '../api/MainApi'
import LectorThumbnail from '../components/LectorThumbnail'

class Home extends React.Component {
  state = {
    slideData: [],
    trackList: [],
    interviewsList: [],
    loaded: false,
    popularsList: [],
    courses: [],
  }

  getTrackList = () => {
    HomeApi.getTrackList().then((response) => {
      console.log("getTrackList", response)
      this.setState({ trackList: response })
    })
  }

  getInterviewsList = () => {
    HomeApi.getInterviewsList().then((response) => {
      console.log("getInterviewsList", response)
      this.setState({ interviewsList: response })
    })
  }

  getCourseList = () => {
    MainApi.getCourseList('?limit=3').then((response) => {
      this.setState({ courses: response })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  getSlideData = () => {
    HomeApi.getSlideData().then((response) => {
      console.log("getSlideData", response)
      this.setState({ slideData: response, loaded: true })
    })
  }

  getPopularsList = () => {
    HomeApi.getPopularsList().then((response) => {
      console.log("getPopularsList", response)
      this.setState({ popularsList: response })
    })
  }

  render() {
    let { slideData, trackList, interviewsList, popularsList, courses } = this.state
    return (
      <div>
        <Helmet
          title="Page"
          meta={[
            { name: 'charSet', content: 'UTF-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { name: 'description', content: 'Page desc' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        />
        <div>
          <header className="main">
            <div className="bar-mobile"><span className="top"></span><span className="middle"></span><span
              className="bottom"></span></div>
            <div className="header">
              <a className="logo" href="/">// Muzium ///</a>
              <nav>
                <ul>
                  <li className="menu-link"><a href="courses">курсы</a></li>
                  <li className="menu-link"><a href="lectors">лекторы</a></li>
                  <li className="menu-link"><a href="review">Рецензии</a></li>
                  <li className="menu-link"><a href="#">Интервью</a></li>
                  <li className="menu-link"><a href="videoblog">видеоблог</a></li>
                  <li className="menu-link"><a href="#">треклисты</a></li>
                  <li className="menu-link"><a href="triko">трико</a></li>
                  <li className="menu-link"><a href="other">разное</a></li>
                  <li className="menu-link"><a href="#">о проекте</a></li>
                  <li>
                    <input id="search" name="search" type="text" placeholder="Что ищем?"/>
                    <div className="border"></div>
                    <input id="search_submit" value="" className="search" data-toggle="off"
                           type="submit"></input>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
          <div id="owl-carousel">
            <div style={{ background: 'url(/static/img/slide-1.jpg)' }} className="info-mob">
              <div className="wrap">
                <p className="review">Рецензия</p>
                <h2 className="hed">«Коко Шанель и Игорь<br/> Стравинский», «Бег на месте»</h2>
                <p className="desc">Два фильма о музыке</p>
                <div className="autor"><img src="/static/img/user-img.png" alt=""
                                            className="user-pic"/><span>Артем Мозговой</span></div>
              </div>
            </div>

            <div style={{ background: 'url(/static/img/slide-1.jpg)' }} className="info-mob">
              <div className="wrap">
                <p className="review">Рецензия</p>
                <h2 className="hed">«Коко Шанель и Игорь<br/> Стравинский», «Бег на месте»</h2>
                <p className="desc">Два фильма о музыке</p>
                <div className="autor"><img src="/static/img/user-img.png" alt=""
                                            className="user-pic"/><span>Артем Мозговой</span></div>
              </div>
            </div>
            <div style={{ background: 'url(/static/img/slide-1.jpg)' }} className="info-mob">
              <div className="wrap">
                <p className="review">Рецензия</p>
                <h2 className="hed">«Коко Шанель и Игорь<br/> Стравинский», «Бег на месте»</h2>
                <p className="desc">Два фильма о музыке</p>
                <div className="autor"><img src="/static/img/user-img.png" alt=""
                                            className="user-pic"/><span>Артем Мозговой</span></div>
              </div>
            </div>
          </div>

          <Slide slide={slideData} loaded={this.state.loaded}/>

          <div className="selector-bg"></div>
          <div className="container content">
            <div className="row first-row">
              <div className="section-title">
                <h3>Популярное</h3>
              </div>
              <div className="media blocks">

                {popularsList.map((item, i) => (
                  <Popular key={i} id={item.id} contentType={item.content_type}/>
                ))}

              </div>
            </div>
            <div className="row content-wrapper">
              <div className="section-title">
                <h3>Язык музыки</h3>
              </div>
              <div className="media blocks">

                {trackList.map((item, i) => (
                  <div key={i} className="col-sm-4">
                    <a className="inform" href="#">
                      <div className="block-with-icon item">
                        <div className="image-src"><img src={item.image}
                                                        alt=""/></div>
                        <div className="information">
                          <div className="crew crew-reverse"></div>
                          <p className="date">{MainApi.getDate(item.date)}</p>
                          <h5 className="title">{item.title}</h5>
                          <p>{this.cleanHtml(item.text_short)}</p>
                          <p className="autor">// Юрий Кудряшов22</p><img
                          src="/static/img/icons/icon-cinema.png" alt=""
                          className="img-pic"/>
                        </div>
                      </div>
                    </a>
                  </div>
                ))}


                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-with-icon item">
                      <div className="image-src"><img src="/static/img/music-2.jpg"
                                                      alt=""/></div>
                      <div className="information">
                        <div className="crew crew-reverse"></div>
                        <p className="date">20 марта 2017</p>
                        <h5 className="title">Restless</h5>
                        <p>Рецензия на запись Первой симфонии Эдуарда Элгара в исполнении
                          Даниэля Баренбойма и Staatskapelle Berlin</p><img
                        src="/static/img/icons/icon-cd.png" alt=""
                        className="img-pic"/>
                        <p className="autor">// Ярослав Тимофеев</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-with-icon item">
                      <div className="image-src"><img src="/static/img/music-3.jpg"
                                                      alt=""/></div>
                      <div className="information">
                        <div className="crew crew-reverse"></div>
                        <p className="date">25 февраля 2016</p>
                        <h5 className="title">«Коко Шанель и Игорь Стравинский», «Бег на
                          месте»</h5>
                        <p>Два фильма о музыке</p><img
                        src="/static/img/icons/icon-cinema.png" alt=""
                        className="img-pic"/>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div className="row content-wrapper">
              <div className="section-title">
                <h3>Курсы</h3>
              </div>
              <div className="media blocks">

                {courses.map((item, i) => (
                  <div key={i} className="col-sm-4">
                    <a className="inform" href={'/courses/' + item.id}>
                      <div className="block-red item">
                        <div className="image-src"><img src={item.image}
                                                        alt=""/></div>
                        <div className="information">
                          <div className="crew"></div>
                          <p className="date">{item.number_lectures} лекции</p>
                          <h5 className="title">{item.title}</h5>
                          <LectorThumbnail small={true} id={item.lecturer}/>
                        </div>
                      </div>
                    </a>
                  </div>
                ))}

                <div className="section-button"><a className="button" href="/courses">Все курсы</a></div>
              </div>
            </div>
            <div className="row content-wrapper">
              <div className="section-title">
                <h3>Интервью</h3>
              </div>
              <div className="media blocks">

                {interviewsList.map((item, i) => (
                  <div key={i} className="col-sm-4">
                    <a className="inform" href="#">
                      <div className="block-blue item">
                        <div className="image-src"><img src={item.image}
                                                        alt=""/></div>
                        <div className="information">

                          <div className="crew crew-reverse"></div>
                          <p className="date">{MainApi.getDate(item.date)}</p>
                          <h5 className="title">{item.title}</h5>
                          <p>{this.cleanHtml(item.text_short)}</p>
                          <p className="autor">// Мария Чалых</p>
                        </div>
                      </div>
                    </a>
                  </div>
                ))}

                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-blue item">
                      <div className="image-src"><img src="/static/img/popular-2.jpg"
                                                      alt=""/></div>
                      <div className="information">

                        <div className="crew crew-reverse"></div>
                        <p className="date">17 июня 2016</p>
                        <h5 className="title">«Классическая музыка конструирует время по
                          собственным законам»</h5>
                        <p>Интервью с музыкальным критиком Ярославом Тимофеевым</p>
                        <p className="autor">// Мария Чалых</p>
                      </div>
                    </div>
                  </a>
                </div>


                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-blue item">
                      <div className="image-src"><img src="/static/img/interview-3.jpg"
                                                      alt=""/></div>
                      <div className="information">

                        <div className="crew crew-reverse"></div>
                        <p className="date">22 апреля 2016</p>
                        <h5 className="title">«Образ взбалмошной женщины мне давался
                          тяжело»</h5>
                        <p>Интервью с Юлией Лежневой</p>
                        <p className="autor">// Мария Чалых</p>
                      </div>
                    </div>
                  </a>
                </div>

              </div>
            </div>
            <div className="row content-wrapper">
              <div className="section-title">
                <h3>Треклисты</h3>
              </div>
              <div className="media blocks">

                {trackList.map((item, i) => (
                  <div key={i} className="col-sm-4">
                    <a className="inform" href="#">
                      <div className="block-with-icon item">
                        <div className="image-src"><img src={item.image}
                                                        alt=""/></div>
                        <div className="information">
                          <div className="crew crew-reverse"></div>
                          <p className="date">{MainApi.getDate(item.date)}</p>
                          <h5 className="title">{item.title}</h5>
                          <p>{this.cleanHtml(item.text_short)}</p>
                          <img src="/static/img/icons/icon-list.png" alt=""
                               className="img-pic"/>
                        </div>
                      </div>
                    </a>
                  </div>
                ))}

                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-with-icon item">
                      <div className="image-src"><img src="/static/img/list-2.jpg"
                                                      alt=""/></div>
                      <div className="information">
                        <div className="crew crew-reverse"></div>
                        <p className="date">3 ноября 2016</p>
                        <h5 className="title">Треклист Народный</h5>
                        <p>Народная музыка в «аранжировках» русских композиторов</p><img
                        src="/static/img/icons/icon-list.png" alt=""
                        className="img-pic"/>
                      </div>
                    </div>
                  </a>
                </div>


                <div className="col-sm-4">
                  <a className="inform" href="#">
                    <div className="block-with-icon item">
                      <div className="image-src"><img src="/static/img/list-3.jpg"
                                                      alt=""/></div>
                      <div className="information">
                        <div className="crew crew-reverse"></div>
                        <p className="date">14 октября 2016</p>
                        <h5 className="title">Треклист “Другое пространство”</h5>
                        <p>Прочь из зоны комфорта</p><img
                        src="/static/img/icons/icon-list.png" alt=""
                        className="img-pic"/>
                      </div>
                    </div>
                  </a>
                </div>

              </div>
            </div>
            <div className="container">
              <div className="section-button"><a className="button">Все треклисты</a></div>
            </div>
            <Footer/>
          </div>

          <Helmet>

            <script src="/static/scripts/jquery.easing.1.3.js"></script>
            <script src="/static/scripts/animate-css.js"></script>
            <script src="/static/scripts/jquery.sldr.js"></script>
            <script src="/static/scripts/jquery-ui.min.js"></script>
          </Helmet>
        </div>

      </div>

    )
  }

  componentDidMount() {
    this.getSlideData()
    this.getTrackList()
    this.getInterviewsList()
    this.getPopularsList()
    this.getCourseList()
  }
}

export default Home
