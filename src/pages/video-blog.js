import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import LectorCourses from '../components/LectorCourses'
import Footer from '../components/footer'

class VideoBlog extends React.Component {
  state = {
    list: [],
  }

  getVideosList = () => {
    MainApi.getVideosList().then((response) => {
      this.setState({ list: response })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {
    let { list } = this.state
    return (
      <Layout>
        <div className="content container videoblog">
          <h3>ВИДЕОБЛОГ</h3>
          <h2>ЯЗЫК МУЗЫКИ</h2>
          <section className="video">
            {list.map((item, i) => (
              <div key={i} className="row item-video">
                <div className="col-sm-6 no-padding">
                  <div className="iframe">
                    <iframe src={item.video} frameBorder="0"
                            allow="autoplay; encrypted-media"
                            allowFullScreen></iframe>
                  </div>
                </div>
                <div className="col-sm-6">
                  <h3>{item.title}</h3>
                  <p className="date">{MainApi.getDate(item.date)}</p>
                  <p className="info" dangerouslySetInnerHTML={{ __html: this.cleanHtml(item.text) }}></p>
                </div>
              </div>
            ))}
          </section>
          <Footer/>
        </div>
      </Layout>
    )
  }

  componentDidMount() {
    this.getVideosList()
  }
}

export default VideoBlog
