import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import LectorCourses from '../components/LectorCourses'
import Footer from '../components/footer'

class Triko extends React.Component {
  state = {
    list: [],
  }

  getTrikoList = () => {
    MainApi.getTrikoList().then((response) => {
      this.setState({ list: response })
    })
  }


  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {
    let { list } = this.state
    return (
      <Layout>

        {list.map((item, i) => (
          <div key={i} className="content container triko">
            <div className="row head">
              <div className="col-sm-9">
                <div className="section-title-small-line">
                  <h6>{item.title}</h6>
                  <div className="line">
                    <hr/>
                    <span>///</span>
                  </div>
                </div>
                <p className="quote" dangerouslySetInnerHTML={{ __html: item.lead }}></p>
              </div>
              <div className="autor autor-rev"><img src={item.author_image} alt="" className="user-pic"/>
                <div className="info-autor"><span>{item.author_name}</span><br/>{MainApi.getDate(item.date)}</div>
              </div>
            </div>
            <div className="body">
              <p className="pre"><strong><i dangerouslySetInnerHTML={{ __html: item.text_short }}></i></strong></p>
              <div className="concert" dangerouslySetInnerHTML={{ __html: item.text }}>
              </div>


              {/*
              <div className="concert">
                <u>Концерт второй</u>
                <p><strong><i>4 июня, Концертный зал имени Чайковского</i></strong></p>
                <p><strong><i> Госоркестр имени Светланова, Камерный хор Московской консерватории, Чулпан Хаматова,
                  Максим
                  Суханов и другие, дирижер Владимир Юровский</i></strong></p>
                <p><strong><i>Прокофьев. «Египетские ночи», музыка к спектаклю</i></strong></p>
                <p><strong><i>Шостакович. «Гамлет», музыка к спектаклю</i></strong></p>


                <div className="pl">
                  <img src="/static/img/Jurowski2_1.jpg"/>
                  <p>В 1932 году Николай Акимов уговорил Шостаковича написать музыку к своему «Гамлету». Спустя два года
                    только что репатриировавшийся Прокофьев создал партитуру к «Египетским ночам» Александра Таирова. У
                    обоих спектаклей, помимо шекспировского текста, было еще два важных общих свойства. Во-первых, они
                    так или иначе фокусировались на теме борьбы за власть. Во-вторых, были звонкими пощечинами
                    общественному вкусу. Акимовскую постановку даже много десятилетий спустя называли самым скандальным
                    «Гамлетом» в истории. «Египетские ночи» вызвали шквал критики за «надругательство над классикой».
                    Сегодня пухлые щечки общественного вкуса снова краснеют от пощечин, действительных и мнимых. Ну а
                    борьба за власть, как известно, актуальна всегда.
                    Есть у этих спектаклей и еще одна общая черта. Оба зацепились за вечность благодаря проницательному
                    выбору композиторов. В партитурах Прокофьева и Шостаковича, кстати, не так уж много борьбы за власть
                    и совсем нет пощечин. Да и какой-то особой актуальности в них, в общем, тоже нет. Их играют, потому
                    что это очень красивая музыка. А там, где есть красота, актуальность уже не актуальна.</p>
                  <a className="button">Подробнее о концерте</a>
                </div>
              </div>
              <div className="concert">
                <u>Концерт третий</u>
                <p><strong><i></i>10 июня, Концертный зал имени Чайковского</strong></p>
                <p><strong><i></i>Николай Луганский (фортепиано), Леонидас Кавакос (скрипка), Готье Капюсон
                  (виолончель)</strong></p>
                <p><strong><i></i> Брамс. Все фортепианные трио</strong></p>

                <div className="pl">
                  <img src="/static/img/JurowskiKavakosKapuson.jpg"/>
                  <p>Еще не угасли восторги по поводу концерта звездного трио Муттер — Харрелл — Бронфман, который мы
                    анонсировали месяц назад, как на ту же сцену выходит новая великолепная тройка. Это своего рода
                    ответ среднего поколения старшему. Снова три крупных независимых солиста из трех стран (пианист,
                    разумеется, опять русский), обладающих редкой способностью, — слушать друг друга. Почему-то
                    музыканты решили выстроить вечер фортепианных трио Брамса с обратным отсчетом — от третьего к
                    первому, от зрелого к юношескому. А ведь в финал принято помещать самые яркие и выигрышные вещи. И
                    вот интересно: то ли это Брамс с каждым следующим трио терял вдохновение, то ли публика с тех пор
                    так деградировала, что больше всего мы обрадуемся самому простому? Остается надеяться, что у
                    музыкантов есть альтернативный ответ. Или три ответа.</p>
                  <a className="button">Подробнее о концерте</a>
                  <div className="autor autor-rev footer"><img src="/static/img/user-img.png" alt=""
                                                               className="user-pic"/>
                    <div className="info-autor"><span>Ярослав Тимофеев</span><br/>
                      26 мая 2017
                    </div>
                  </div>
                </div>
              </div>*/}
            </div>
            <Footer/>
          </div>
        ))}
      </Layout>
    )
  }

  componentDidMount() {
    this.getTrikoList()
  }
}

export default Triko
