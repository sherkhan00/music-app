import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import Footer from '../components/footer'
import ReviewNote from '../components/ReviewNote'

class Review extends React.Component {
  state = {
    list: [],
    firstMediaBlock: [],
    secondMediaBlock: [],
    anonce: {id: '', title: '', image: '', lead: '<p></p>', text_short: '<p></p>', date: '', author_name: '' },
  }

  getReviewsList = () => {
    MainApi.getReviewsList().then((response) => {
      let anonce = {}, firstBlock = [], secondBlock = []
      let list = []
      let childlist = []
      let index = 0
      response.map((item, i) => {
          if (i === 0) {
            anonce = {
              id: item.id,
              title: item.title,
              image: item.image,
              text_short: item.text_short,
              date: item.date,
              author_name: item.author_name,
            }
          } else {
            childlist.push(item)
            list[index] = childlist
            let indexInner = i + 1
            if (indexInner % 3 === 0) {
              index++
              childlist = []
            }
          }
        },
      )
      this.setState({ anonce: anonce, list: list })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {
    let url = this.props.location.pathname
    if(url !== '/review') {
      let id = this.props.location.pathname.replace('/review/', '')
      return <ReviewNote id={id}/>
    }

    let { anonce, list } = this.state
    console.log("anonce", anonce)
    return (
      <Layout>
        <div className="content container review">
          <div className="section-title-inner">
            <h6>Рецензий</h6>
            <ul className="heading-list">
              <li><a href="" className="active">Все</a></li>
              <li><a href="">Аудиорелизы</a></li>
              <li><a href="">Фильмы</a></li>
            </ul>
          </div>
          <div className="media">
            <div className="col-sm-5 no-padding-left media-img"><img src={anonce.image} alt="youth"
                                                                     className="poster"/></div>
            <div className="col-sm-7 media-body">
              <h4>{anonce.title}</h4>
              <img src="/static/img/icons/icon-cinema.png" alt="" className="img-pic"/>
              {/*<p className="subtitle">{this.cleanHtml(anonce.lead)}</p>*/}
              <div className="desc">
                <p dangerouslySetInnerHTML={{ __html: this.cleanHtml(anonce.text_short) }}></p>
              </div>

              <div className="section-button media"><a href={'/review/' + anonce.id} className="button">ЧИТАТЬ</a>
                <p>{MainApi.getDate(anonce.date)} // {anonce.author_name}</p>
              </div>
            </div>
          </div>
          <div className="clear"></div>
          <div className="section-title"></div>
          {list.map((item, i) => (
            <div key={i} className="media blocks">
              {item.map((itemInner, i2) => (
                <div key={i2} className="col-sm-4">
                  <a className="inform" href={'/review/' + itemInner.id}>
                    <div className="block-with-icon item">
                      <div className="image-src"><img src={itemInner.image} alt=""/></div>
                      <div className="information">
                        <div className="crew crew-reverse"></div>
                        <p className="date">{MainApi.getDate(itemInner.date)}</p>
                        <h5 className="title">{itemInner.title}</h5>
                        <p dangerouslySetInnerHTML={{ __html: itemInner.text_short }}></p>
                        <p className="autor">// {itemInner.author_name}</p><img src="/static/img/icons/icon-cinema.png"
                                                                           alt=""
                                                                           className="img-pic"/>
                      </div>
                    </div>
                  </a>
                </div>
              ))}
            </div>
          ))}
          <div className="clear"></div>
          <Footer/>
        </div>
      </Layout>
    )
  }

  componentDidMount() {
    this.getReviewsList()
  }
}

export default Review
