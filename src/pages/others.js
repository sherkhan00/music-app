import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import LectorCourses from '../components/LectorCourses'
import Footer from '../components/footer'

class Others extends React.Component {
  state = {
    list: [],
  }

  getOthersList = () => {
    MainApi.getOthersList().then((response) => {
      this.setState({ list: response })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {
    let { list } = this.state
    return (
      <Layout>
        <div className="content container triko other">

          {list.map((item, i) => (
            <div key={i}>
              <div className="row head">
                <div className="col-sm-9">
                  <div className="section-title-small-line">
                    <h6>{item.title}</h6>
                    <div className="line">
                      <hr/>
                      <span>///</span>
                    </div>
                  </div>
                  <p className="quote">«Разговоры о том, что классическая музыка — это удел старпёров, не имеют никакого
                    отношения к пониманию её сути»</p>
                </div>
                <div className="autor autor-rev">{MainApi.getDate(item.date)}</div>
              </div>
              < div className='body'>
                <p className='pre'><strong><i dangerouslySetInnerHTML={{ __html: this.cleanHtml(item.text_short) }}></i></strong>
                </p>
                <div className='pl'>
                  <img src={item.image}/>
                  <p dangerouslySetInnerHTML={{ __html: item.text}}>
                  </p>

                </div>
              </div>
            </div>
          ))}


        </div>
        <Footer/>
      </Layout>
    )
  }

  componentDidMount() {
    this.getOthersList()
  }
}

export default Others
