import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'

import '../public/src/css/fonts.css'
import '../public/src/css/bootstrap.css'
import '../public/src/css/main.css'
import '../public/src/css/owl.carousel.css'
import '../public/src/css/animate.min.css'

const CourseList = ({ children, data }) => (
  <div className="row item-blocks">
    <div className="media blocks">
      {children()}
    </div>
  </div>
)


export default CourseList
