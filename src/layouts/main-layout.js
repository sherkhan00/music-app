import React from 'react'
import { push } from 'gatsby-link'
import Helmet from 'react-helmet'

class Layout extends React.Component {

  render() {
    return (
      <div>
        <header className="inner">
          <div className="bar-mobile inner"><span className="top"></span><span className="middle"></span><span
            className="bottom"></span></div>
          <div className="header">
            <a className="logo" href="/">// Muzium ///</a>
            <nav>
              <ul>
                <li className="menu-link"><a href="/courses">курсы</a></li>
                <li className="menu-link"><a href="/lectors">лекторы</a></li>
                <li className="menu-link"><a href="/review">Рецензии</a></li>
                <li className="menu-link"><a href="#">Интервью</a></li>
                <li className="menu-link"><a href="/video-blog">видеоблог</a></li>
                <li className="menu-link"><a href="#">треклисты</a></li>
                <li className="menu-link"><a href="/triko">трико</a></li>
                <li className="menu-link"><a href="/others">разное</a></li>
                <li className="menu-link"><a href="#">о проекте</a></li>
                <li>
                  <input id="search" name="search" className="search-black" type="text" placeholder="Что ищем?"/>
                  <div className="border"></div>
                  <input id="search_submit" value="" className="search  search-black" data-toggle="off"
                         type="submit"></input>
                </li>
              </ul>
            </nav>
          </div>
        </header>
        <div>
          {this.props.children}

        </div>
      </div>
    )
  }
}

export default Layout
