import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'


const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title="Page"
      meta={[
        { name: 'charSet', content: 'UTF-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'description', content: 'Page desc' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <div>
      {children()}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
    query SiteTitleQuery {
    site {
    siteMetadata {
    title
  }
  }
  }
    `
