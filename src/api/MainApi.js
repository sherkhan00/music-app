const API_URL = `http://31.192.109.24/api/`

export class MainApi {

  static getDate(date) {
    var event = new Date(date)
    let mothA = 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'.split(',')
    return event.getDay() + ' ' + mothA[event.getMonth()] + ' ' + event.getFullYear()
  }

  static unifetch = (URL) => {
    return fetch(API_URL + URL,
      {
        method: 'GET',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
        body: null,
      }).then(response => response.json())
      .then((responseJson) => {
        return responseJson
      })
  }

  static getCourseList(limit = '') {
    try {
      return this.unifetch(`courses/${limit}`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (onLogin): ' + error.message)
    }

  }

  static getEpochesList() {
    try {
      return this.unifetch(`epoches/`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (onLogin): ' + error.message)
    }

  }

  static getLectorsList() {
    try {
      return this.unifetch(`lecturers/`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (onLogin): ' + error.message)
    }

  }


  static getLectorCoursesList(lector) {
    try {
      return this.unifetch(`courses/?lecturer=${lector}`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (onLogin): ' + error.message)
    }

  }

  static getReviewsList() {
    try {
      return this.unifetch(`critiques/?limit=7`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getReviewsList): ' + error.message)
    }

  }

  static getAnonceList() {
    try {
      return this.unifetch(`announcement/?limit=1`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getAnonceList): ' + error.message)
    }
  }

  static getVideosList() {
    try {
      return this.unifetch(`blog_video/`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getVideosList): ' + error.message)
    }
  }

  static getOthersList() {
    try {
      return this.unifetch(`others/`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getOthersList): ' + error.message)
    }
  }

  static getLecturer(id) {
    try {
      return this.unifetch(`lecturers/${id}/`).then((responseJson) => {
        return responseJson
      })
    } catch (error) {
      console.log('Error when call API (getLecturer): ' + error.message)
    }
  }

  static getCourse(id) {
    if (id === '')
      return
    try {
      return this.unifetch(`courses/${id}/`).then((responseJson) => {
        return responseJson
      })
    } catch (error) {
      console.log('Error when call API (getLecturer): ' + error.message)
    }
  }

  static getCourseLectures(id) {
    if (id === '')
      return
    try {
      return this.unifetch(`lectures/?course=${id}`).then((responseJson) => {
        return responseJson
      })
    } catch (error) {
      console.log('Error when call API (getLecturer): ' + error.message)
    }
  }

  static getCourseTracks(id) {
    if (id === '')
      return
    try {
      return this.unifetch(`course_tracks/?course=${id}`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getLecturer): ' + error.message)
    }
  }

  static getReview(id) {
    if (id === '')
      return
    try {
      return this.unifetch(`critiques/${id}/`).then((responseJson) => {
        return responseJson
      })
    } catch (error) {
      console.log('Error when call API (getLecturer): ' + error.message)
    }
  }

  static getTrikoList() {
    try {
      return this.unifetch(`announcement/?limit=3`).then((responseJson) => {
        return responseJson.results
      })
    } catch (error) {
      console.log('Error when call API (getTrikoList): ' + error.message)
    }
  }
}



