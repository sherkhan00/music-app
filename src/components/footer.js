import React from 'react'
import Link from 'gatsby-link'


const Footer = ({ siteTitle }) => (
  <footer>
    <nav className="footer">
      <ul>
        <li><a href="/courses">курсы</a></li>
        <li><a href="/lectors">лекторы</a></li>
        <li><a href="/review">Рецензии</a></li>
        <li><a href="#">Интервью</a></li>
        <li><a href="/video-blog">видеоблог</a></li>
        <li><a href="#">треклисты</a></li>
        <li><a href="/triko">трико</a></li>
        <li><a href="/others">разное</a></li>
        <li><a href="#">о проекте</a></li>
        <li>
        </li>
      </ul>
    </nav>
    <div className="footer-bottom">
      <div className="logo">// MUZIUM //</div>
      <div className="section-social">
        <p className="blog">АВТОРСКИЙ БЛОГ О КЛАССИЧЕСКОЙ МУЗЫКЕ</p>
        <a className="first" href=""><img src="/static/img/icons/facebook.png"/></a>
        <a href=""><img src="/static/img/icons/vk.png"/></a>
        <a href=""><img src="/static/img/icons/yt.png"/></a>
      </div>
      <div className="section-partners">
        <p className="partners">ПАРТНЕРЫ ПРОЕКТА:</p>
        <img className="filarmony" src="/static/img/icons/m.png"/>
        <img src="/static/img/icons/universal.png"/>
      </div>
    </div>
  </footer>
)

export default Footer
