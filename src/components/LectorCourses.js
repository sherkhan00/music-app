import React from 'react'

import { MainApi } from '../api/MainApi'

class LectorCourses extends React.Component {
  state = {
    list: [],
  }

  getLectorCoursesList = () => {
    MainApi.getLectorCoursesList(this.props.lector).then((response) => {
      this.setState({ list: response })
    })
  }

  render() {
    let { list } = this.state
    return (
      <p className="tags">
        <strong>Курсы: </strong>
        {list.map((item, i) => (
          <a key={i} href={'courses/' + item.id}>{item.title}, </a>
        ))}
      </p>
    )
  }

  componentDidMount() {
    this.getLectorCoursesList()
  }
}

export default LectorCourses
