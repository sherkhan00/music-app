import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import LectorThumbnail from '../components/LectorThumbnail'
import Footer from '../components/footer'


class ReviewNote extends React.Component {
  state = {
    review: {},
    loading: true,
  }

  // Tracks
  getReview = () => {
    MainApi.getReview(this.props.id).then((response) => {
      this.setState({ review: response, loading: false })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {

    let { review, loading } = this.state
    if (loading)
      return <p></p>

    return (
      <Layout>
        <div className="content container review-note">
          <div className="row">
            <div className="col-sm-7 no-padding-left">
              <div className="section-title-with-line">
                <h6>{review.title}</h6>
              </div>
              <p className="quote" dangerouslySetInnerHTML={{ __html: this.cleanHtml(review.text_short) }}></p>
              <div className="autor autor-rev"><img src={review.author_image} alt="" className="user-pic"/>
                <div className="info-autor"><span>{review.author_name}</span><br/>{MainApi.getDate(review.date)}</div>
              </div>

              <div className="body">
                <p dangerouslySetInnerHTML={{ __html: this.cleanHtml(review.text) }}></p>
              </div>
            </div>
            <div className="col-sm-5 aside">
              <div className="media-img">
                <img src={review.image}/>
                <div className="section-button">
                  <a href={review.album_itunes} className="button button-with-icon">АЛЬБОМ В iTUNES<img
                    src="/static//img/icons/icon-cd.png"
                    alt="" className="img-pic"/></a>
                  <a href={review.album_yandex_music} className="button button-with-icon">АЛЬБОМ В ЯНДЕКС.МУЗЫКА<img
                    src="/static//img/icons/icon-cd.png" alt="" className="img-pic"/></a>
                </div>
                <div className="blockquote">
                  <p>//</p>
                  <blockquote>
                    Удельный вес музыки Элгара в Великобритании в десятки раз превышает присутствие её в остальном
                    мире. Кто тут перегибает или недогибает палку — судить не нам. А, например, Даниэлю Баренбойму:
                    он явно на стороне англичан.
                  </blockquote>
                  <p>//</p>
                </div>
                <div className="video">
                  <iframe width="420" height="315"
                          src="https://www.youtube.com/embed/tgbNymZ7vqY">
                  </iframe>
                  <p>Jacqueline du Pré: Edward Elgar — Cello Concerto</p>
                </div>
              </div>
            </div>
          </div>
          <Footer/>
        </div>

      </Layout>
    )
  }

  componentDidMount() {
    this.getReview()
  }
}

export default ReviewNote
